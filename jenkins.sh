docker container run -d \
    -p 8081:8080 \
    -v jenkins:/var/jenkins_home \
    --name jenkins-local \
    jenkins/jenkins:lts
