defmodule CamcookiesWeb.OfferChannel do
  use Phoenix.Channel
  require Logger
  def join("offer", _payload, socket) do
    {:ok, socket}
  end
  def handle_in("offer", payload, socket) do
    broadcast!(socket, "new_msg", %{body: payload})
    Logger.info ":: Example:Broadcast receive a message!::"
    {:noreply, socket}
  end
end
