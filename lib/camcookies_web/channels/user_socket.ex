defmodule CamcookiesWeb.UserSocket do
  alias CamcookiesWeb.Channel, as: Channel

  use Phoenix.Socket
  require Logger
  ## Channels
  # channel "room:*", CamcookiesWeb.RoomChannel
  channel "video:*", Channel

#  channel "broadcaster", CamcookiesWeb.ExampleChannel
#  channel "offer", CamcookiesWeb.OfferChannel
#  channel "answer", CamcookiesWeb.AnswerChannel
#  channel "candidate", CamcookiesWeb.CandidateChannel
#  channel "disconnect", CamcookiesWeb.DisconnectChannel

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.
  @impl true
  def connect(_params, socket, _connect_info) do
    {:ok, socket}
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     CamcookiesWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  @impl true
  def id(_socket), do: nil
end


defmodule CamcookiesWeb.Channel do
  use Phoenix.Channel
  require Logger


  def join("video:peer2peer", _message, socket) do
    {:ok, socket}
  end

  def handle_in("peer-message", %{"body" => body}, socket) do
    broadcast_from!(socket, "peer-message", %{body: body})
    {:noreply, socket}
  end

#  def join("vidcity:" <> challenge_id, _payload, socket) do
#    IO.inspect challenge_id
#    {:ok, socket}
#  end
#
#  def handle_in("vidcity:broadcaster", payload, socket) do
#    Camcookies.KeyValue.put(:broadcaster_ref, socket.ref)
#    broadcast!(socket, "vidcity:broadcaster", %{})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end
#
#  def handle_in("vidcity:watcher", payload, socket) do
#    broadcast!(socket,"vidcity:broadcaster", %{body: "h"})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end
#
#  def handle_in("vidcity:offer", payload, socket) do
#    broadcast!(socket, "new_msg", %{body: payload})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end
#
#  def handle_in("vidcity:answer", payload, socket) do
#    broadcast!(socket, "new_msg", %{body: payload})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end
#
#  def handle_in("vidcity:candidate", payload, socket) do
#    broadcast!(socket, "new_msg", %{body: payload})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end
#
#  def handle_in("vidcity:disconnect", payload, socket) do
#    broadcast!(socket, "new_msg", %{body: payload})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end
#
#  def handle_in("vidcity:example", payload, socket) do
#    broadcast!(socket, "new_msg", %{body: payload})
#    Logger.info ":: Broadcaster receive a message!::"
#    {:noreply, socket}
#  end

end
