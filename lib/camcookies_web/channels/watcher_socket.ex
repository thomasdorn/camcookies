defmodule CamcookiesWeb.WatcherChannel do
  use Phoenix.Channel
  require Logger
  def join("watcher", _payload, socket) do
    {:ok, socket}
  end
  def handle_in("watcher", payload, socket) do
    broadcast!(socket, "new_msg", %{body: payload})
    Logger.info ":: Example:Broadcast receive a message!::"
    {:noreply, socket}
  end
end
