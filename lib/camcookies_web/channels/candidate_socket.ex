defmodule CamcookiesWeb.CandidateChannel do
  use Phoenix.Channel
  require Logger
  def join("candidate", _payload, socket) do
    {:ok, socket}
  end
  def handle_in("candidate", payload, socket) do
    broadcast!(socket, "new_msg", %{body: payload})
    Logger.info ":: Example:Broadcast receive a message!::"
    {:noreply, socket}
  end
end
