defmodule CamcookiesWeb.DisconnectChannel do
  use Phoenix.Channel
  require Logger
  def join("disconnect", _payload, socket) do
    {:ok, socket}
  end
  def handle_in("disconnect", payload, socket) do
    broadcast!(socket, "new_msg", %{body: payload})
    Logger.info ":: Example:Broadcast receive a message!::"
    {:noreply, socket}
  end
end
