defmodule CamcookiesWeb.UsersController do
  use CamcookiesWeb, :controller
  def test(conn, _) do
    username = Camcookies.Service.User.create("hello", "no") |> IO.inspect
    {:ok, weather}  = %Camcookies.Weather{city: "pittsburgh", temp_lo: 89} |> Camcookies.Repo.insert
    conn
    |> Plug.Conn.put_resp_content_type("application/json")
    |> Plug.Conn.send_resp(200, Poison.encode!(weather))
  end

  def login(conn, params) do
    username = params["username"];
    password = params["password"];
    {:ok, response} = Camcookies.Service.User.login(username, password)
    conn
    |> Plug.Conn.send_resp(200, Poison.encode!(%{
      "response" => response
    }))
  end

  def create(conn, params) do
    username = params["username"];
    password = params["password"];
    fullname = params["fullname"];
    {:ok, response} = Camcookies.Service.User.create(username, password, fullname)
    conn
    |> Plug.Conn.send_resp(200, Poison.encode!(%{
      "response" => response
    }))
  end
end

defmodule CamcookiesWeb.Auth.Jwt do
  use Joken.Config
  def generate() do
    {:ok, token, attr} = generate_and_sign()
    token
  end
  def verify_token(conn) do
    conn = Plug.Conn.fetch_query_params(conn)
    token = Map.fetch!(conn.params, "token")
    verify = fn
      {:ok, _attr} ->
        IO.puts "Jwt Valid"
        true
      {:error, _error} ->
        IO.puts "Jwt Invalid"
        false
    end
    verify.(verify_and_validate(token))
  end
end
