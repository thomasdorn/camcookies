defmodule Camcookies.Users do
  use Ecto.Schema
  @primary_key {:id, :binary_id, autogenerate: true}
  @derive {Poison.Encoder, only: [:username]}
  schema "users" do
    field :username,  :string
    field :password, :string
    field :fullname, :string
    field :authlevel, :string
  end
end
