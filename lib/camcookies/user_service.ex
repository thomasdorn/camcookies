defmodule Camcookies.Service.User do
  alias Camcookies.Service.Worker, as: Worker
  @pool_size 1
  def child_spec(_) do
    :poolboy.child_spec(
      __MODULE__,
      [
        name: {:local, __MODULE__},
        worker_module: Worker,
        size: @pool_size,
        max_overflow: 2
      ],
      []
    )
  end
  def create(username, password, fullname) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
         {:ok, result} = Worker.create_new_user(worker_pid, username, password, fullname)
      end
    )
  end
  def login(username, password) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid ->
        {:ok, result} = Worker.login_user(worker_pid, username, password)
      end
    )
  end
end


defmodule Camcookies.Service.Worker do
  alias CoinplotCollector.Auth.Jwt, as: Jwt
  import Ecto.Query, only: [from: 2]
  use GenServer
  def start_link(name) do
    GenServer.start_link(
      __MODULE__,
      []
    )
  end
  @impl GenServer
  def init(_name) do
    {:ok, []}
  end
  def create_new_user(pid, username, password, fullname) do
    GenServer.call(pid, {:create_new_user, username, password, fullname})
  end
  def login_user(pid, username, password) do
    GenServer.call(pid, {:login_user, username, password})
  end
  @impl GenServer
  def handle_call({:create_new_user, username, password, fullname}, _, state) do
    query = from u in "users",
                 where: u.username == ^username
    cursor = Camcookies.Repo.all(query)
    case_result = case Enum.count(cursor) do
      0 ->
        %Camcookies.Users{fullname: fullname, username: username, password: password} |> Camcookies.Repo.insert
      _ ->
        {:ok,
          %{
            "status" => "failed",
            "message" => "user found"
          }
        }
    end
    {:reply, case_result, [:create_new_user]}
  end


  @impl GenServer
  def handle_call({:login_user, username, password}, _, state) do
    query = from u in "users",
                 where: u.username == ^username,
                 where: u.password == ^password

    cursor = Camcookies.Repo.all(query)
    case_result = case Enum.count(cursor) do
      1 ->
        {:ok,
           %{
            "status" => "success",
            "message" => "user login successful"
           }
        }
      _ ->
        {:ok,
          %{
            "status" => "failed",
            "message" => "login failed"
          }
        }
    end
    {:reply, case_result, [:login_user]}
  end

end
