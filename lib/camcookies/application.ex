defmodule Camcookies.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  use Supervisor

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      CamcookiesWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Camcookies.PubSub},
      # Start the Endpoint (http/https)video
      CamcookiesWeb.Endpoint,
      # Start a worker by calling: Camcookies.Worker.start_link(arg)
      # {Camcookies.Worker, arg}
      worker(Camcookies.Repo, []),
      Camcookies.Service.User,
      Camcookies.KeyValue,
    ]
    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Camcookies.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updatedasdasd
  def config_change(changed, _new, removed) do
    CamcookiesWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

defimpl String.Chars, for: BSON.ObjectId do
  def to_string(object_id), do: Base.encode16(object_id.value, case: :lower)
end
