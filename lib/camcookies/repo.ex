defmodule Camcookies.Repo do
  use Ecto.Repo,
      otp_app: :camcookies,
      adapter: Mongo.Ecto
end