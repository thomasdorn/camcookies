defmodule CoinplotCollector.Auth.Jwt do
  use Joken.Config

  def generate() do
    {:ok, token, attr} = generate_and_sign()
    token
  end

  def verify_token(conn) do
    conn = Plug.Conn.fetch_query_params(conn)
    token = Map.fetch!(conn.params, "token")
    verify = fn
      {:ok, _attr} ->
        IO.puts "Jwt Valid"
        true
      {:error, _error} ->
        IO.puts "Jwt Invalid"
        false
    end
    verify.(verify_and_validate(token))
  end
end
