# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :camcookies, CamcookiesWeb.Endpoint,
  url: [host: "localhost"],
  check_origin: ["https://dornlabs.com"],
  secret_key_base: "M8T1i3BEY1BTWS3F/UwT6BzfMjPMZYvVXyMdPEXukl4Hj9LGy9LPI9JhteBeHMMK",
  render_errors: [view: CamcookiesWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Camcookies.PubSub,
  live_view: [signing_salt: "e1C248Rx"],
  check_origin: false


# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :camcookies, Camcookies.Repo,
       adapter: Mongo.Ecto,
       database: "camcookies",
#       username: "mongodb",
#       password: "mongodb",
       hostname: "localhost"

config :joken, default_signer: "secret"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
