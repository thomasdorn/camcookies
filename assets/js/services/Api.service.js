import axios from 'axios';
const API_ENDPOINT_MAP = {
    CREATE_USER: '/users/create',
    LOGIN_USER: '/users/login'
}
const ApiService = {
    loginUser: async function (username, password) {
        const result = await axios.post(API_ENDPOINT_MAP.LOGIN_USER, {
            username,
            password,
        }, {
            headers: {
                "X-CSRF-Token": localStorage.getItem("cross-token")
            }
        });
        return result.data;
    },
    createUser: async function (fullname, username, password) {
        const result = await axios.post(API_ENDPOINT_MAP.CREATE_USER, {
            fullname,
            username,
            password,
        }, {
            headers: {
                "X-CSRF-Token": localStorage.getItem("cross-token")
            }
        });
        return result.data;
    },
}
export default ApiService;
