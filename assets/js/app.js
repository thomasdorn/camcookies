import "../css/app.scss"
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "phoenix_html"
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import Router from "vue-router";
import Login from "./components/Login";
import App from "./components/App";
import Home from "./components/Home";
import Register from "./components/Register";
import Profile from "./components/Profile";
import Room from "./components/Room";
import Broadcast from "./components/Broadcast";

Vue.use(Router);

export const router = new Router({
    routes: [
        {
            name:"Home",
            path: "/",
            component: Home,
        },
        {   name: 'Register',
            path: "/register",
            component: Register,
        },
        {   name: 'Login',
            path: "/login",
            component: Login,
        },
        {   name: 'Profile',
            path: "/profile",
            component: Profile,
        },
        {
            name: 'Room',
            path: "/room",
            component: Room,
        },
        {
            name: 'Broadcast',
            path: "/broadcast",
            component: Broadcast,
        },
        {
            name: 'RtcDemo',
            path: "/rtcdemo",
            component: RtcDemo,
        },
    ]
});


// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
import { Socket } from "phoenix"
import RtcDemo from "./components/RtcDemo";
Vue.use(VueRouter)

new Vue({
    router,
    render: h => h(App)
}).$mount("#app")
