#!/usr/bin/env bash
# exit on error
mix local.hex --force
mix local.rebar --force
mix archive.install hex phx_new 1.5.7 --force
set -o errexit
export SECRET_KEY_BASE=pGl2Fn5V+ZtEzCA7PbcWv3qUkmV7W/qfyHQMIj9C3ZT9V6dT0ceBUh1JTuCV4Rn8
# Initial setup
mix deps.get
mix deps.get --only prod
MIX_ENV=prod mix compile

# Compile assets
npm install --prefix ./assets
npm run deploy --prefix ./assets
mix phx.digest

# Build the release and overwrite the existing release directory
MIX_ENV=prod mix release --overwrite
mix release --overwrite
