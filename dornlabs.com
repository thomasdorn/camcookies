#server {
#    listen 443 ssl http2;
#    listen [::]:443 ssl http2;
#
#    server_name dornlabs.com www.dornlabs.com;
#
#    # special config for EventSource to disable gzip
#    location / {
#        proxy_http_version 1.1;
#        gzip off;
#        proxy_set_header X-Real-IP $remote_addr;
#        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#        proxy_set_header HOST $http_host;
#        proxy_set_header X-NginX-Proxy true;
#        proxy_pass http://localhost:4000;
#        proxy_redirect off;
#    }
#
#
#    ssl_certificate /etc/letsencrypt/live/dornlabs.com-0001/fullchain.pem; # managed by Certbot
#    ssl_certificate_key /etc/letsencrypt/live/dornlabs.com-0001/privkey.pem; # managed by Certbot
#}

upstream phoenix {
  server 127.0.0.1:4000 max_fails=5 fail_timeout=60s;
}

server {
  server_name dornlabs.com www.dornlabs.com;
      listen 443 ssl http2;
    listen [::]:443 ssl http2;
      ssl_certificate /etc/letsencrypt/live/dornlabs.com-0001/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/dornlabs.com-0001/privkey.pem; # managed by Certbot
  location /socket {
    allow all;

    client_max_body_size 50M;

    # a hack to enforce capitalized response header    
    #more_clear_headers 'Connection';
    #more_clear_headers 'connection';
    #more_set_input_headers 'Connection';
    #more_clear_input_headers 'Connection';
    #more_set_headers 'Connection: Upgrade';
    #add_header Connection "Upgrade" always;
    
    proxy_set_header Proxy "";

    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header X-Cluster-Client-Ip $remote_addr;

    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";

    # allow remote connections
    proxy_set_header Origin '';

    proxy_pass http://phoenix;
  }

  location / {
    allow all;

    client_max_body_size 50M;

    proxy_set_header Proxy "";

    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header X-Cluster-Client-Ip $remote_addr;

    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";

    # allow remote connections
    proxy_set_header Origin '';

    proxy_pass http://phoenix;
  }
}


server {
    if ($host = dornlabs.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    listen [::]:80;

    server_name dornlabs.com;
    return 404; # managed by Certbot


}

